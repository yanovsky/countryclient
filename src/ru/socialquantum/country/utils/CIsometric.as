package ru.socialquantum.country.utils
{
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	/**
	 * ...
	 * @author Maxim I. Yanovsky
	 */
	public class CIsometric
	{
		public static const CELL_WIDTH : Number = 98;
		public static const CELL_HEIGHT : Number = 49;
		
		public function CIsometric()
		{
		
		}
		
		/**
		 * Перевод координат из системы игры в систему отображения на экране
		 * @param	pnt
		 * @return
		 */
		public static function normalToIsometric(pnt : Point) : Point
		{
			return new Point(
			(pnt.y + pnt.x) * CELL_WIDTH / 2,
			(pnt.y - pnt.x) * CELL_HEIGHT / 2);
		}
		
		public static function isometricToNormal(pnt : Point) : Point
		{
			return new Point(
			-pnt.y / CELL_HEIGHT + pnt.x / CELL_WIDTH,
			pnt.x / CELL_WIDTH + pnt.y / CELL_HEIGHT);
		}
		
		/**
		 * Нарисовать прямоугольник заданный в координатах игры
		 * @param	graphics
		 * @param	rect
		 */
		public static function drawIsometricRect(graphics : Graphics, rect : Rectangle) : void
		{
			var pnt : Point = normalToIsometric(rect.topLeft);
			graphics.moveTo(pnt.x, pnt.y);
			
			pnt = normalToIsometric(new Point(rect.right, rect.top));
			graphics.lineTo(pnt.x, pnt.y);
			
			pnt = normalToIsometric(rect.bottomRight);
			graphics.lineTo(pnt.x, pnt.y);
			
			pnt = normalToIsometric(new Point(rect.left, rect.bottom));
			graphics.lineTo(pnt.x, pnt.y);
			
			pnt = normalToIsometric(rect.topLeft);
			graphics.lineTo(pnt.x, pnt.y);
		}
	}

}
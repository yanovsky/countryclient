package ru.socialquantum.country.utils
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.net.URLRequest;
	import flash.system.LoaderContext;
	import flash.utils.Dictionary;
	
	/**
	 * ...
	 * @author Maxim I. Yanovsky
	 */
	public class CBitmapStorage
	{
		private static var _mPool : Dictionary = new Dictionary();
		
		public function CBitmapStorage()
		{
		
		}
		
		/**
		 * Функция получения необходимой bitmapdata
		 * @param	url путь до необходимого изображения относительно корня папки медийного хранилища
		 * @param	callback функция принимающая BitmapData единственным аргументом
		 */
		public static function getBitmapData(url : String, callback : Function) : void
		{
			/**
			 * Суть такова:
			 * смотрим в пул, если там то что надо - возвращаем bitmapdata
			 * если там Loader - значит кто-то уже попросил и грузится, подписываемся тоже и когда завершится загрузка, возвращаем bitmapdata
			 * если там пусто - создаем Loader, кладем в пул, подписываемся, по загрузке кладем в пул bitmapdata вместо Loader, возвращаем bitmapdata
			 */
			var loader : Loader;
			var obj : Object = _mPool[url];
			if (obj == null)
			{
				loader = new Loader();
				_mPool[url] = loader;
				loader.load(new URLRequest("media/" + url));
				obj = loader;
			}
			
			if (obj is Loader)
			{
				loader = obj as Loader;
				//Замыкание для удобства. Все равно я отписываюсь внутри, так что память не потечет
				loader.contentLoaderInfo.addEventListener(Event.COMPLETE, function onLoadedClosure(e : Event) : void
					{
						loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onLoadedClosure);
						_mPool[url] = (loader.content as Bitmap).bitmapData;
						callback(_mPool[url]);
					});
			}
			else if (obj is BitmapData)
			{
				callback(obj);
			}
		}
	}

}
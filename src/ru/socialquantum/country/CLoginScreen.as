package ru.socialquantum.country
{
	import fl.controls.Button;
	import fl.controls.TextInput;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import ru.socialquantum.country.events.CUiEvent;
	/**
	 * Экран логина
	 * Юзер выбирает свое имя которое будет являться его идентификатором на сервере и нажимает на кнопку login
	 * @author Maxim I. Yanovsky
	 */
	
	[Event(name="CUiEvent.LOGIN_CLICK", type="ru.socialquantum.country.events.CUiEvent")]
	public class CLoginScreen extends Sprite
	{
		private var _mInput : TextInput;
		private var _mUid : String;
		
		public function CLoginScreen()
		{
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		/**
		 * Имя игрока которое ввел пользователь
		 */
		public function get uid() : String
		{
			return _mUid;
		}
		
		private function onAddedToStage(e : Event) : void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			_mInput = new TextInput();
			_mInput.width = 100;
			_mInput.x = (stage.stageWidth - _mInput.width) / 2;
			_mInput.y = (stage.stageHeight - _mInput.height) / 2;
			addChild(_mInput);
			_mInput.text = "Player";
			
			var btn : Button = new Button();
			btn.label = "Login";
			btn.x = (stage.stageWidth - btn.width) / 2;
			btn.y = _mInput.y + _mInput.height + 5;
			addChild(btn);
			btn.addEventListener(MouseEvent.CLICK, onLoginClick);
		}
		
		private function onLoginClick(e : Event) : void
		{
			if (_mInput.text.length)
			{
				_mUid = _mInput.text;
				dispatchEvent(new CUiEvent(CUiEvent.LOGIN_CLICK));
			}
		}
		
		
	
	}

}
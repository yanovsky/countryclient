package ru.socialquantum.country 
{
	import flash.errors.*;
	import flash.events.*;
	import flash.net.Socket;
	import flash.utils.Dictionary;
	import flash.utils.getTimer;
	
	/**
	 * Класс через который происходит общение клиента с сервером (через метод send)
	 */
	public class CCustomSocket extends Socket
	{
		private var _mUid : String;
		private var _mQueue : Dictionary = new Dictionary();
		private var _mCounter : int = 0;
		
		public function CCustomSocket(uid : String, host : String = null, port : uint = 0)
		{
			super();
			_mUid = uid;
			configureListeners();
			if (host && port)
			{
				super.connect(host, port);
			}
		}
		
		/**
		 * Функция вызова метода API
		 * @param	method имя метода API
		 * @param	callback функция-обработчик ответа с сервера (принимает XML в качестве единственного аргумента)
		 * @param	...params параметры метода (с сохранением порядка)
		 */
		public function send(method : String, callback : Function, ... params) : void
		{
			/**
			 * ts - уникальный (во время сессии) идентификатор запроса, чтобы найти необходимый callback при получении ответа от сервера
			 */
			var ts : int = getTimer() + _mCounter;
			_mCounter++;
			_mQueue[ts] = callback;
			params.unshift(_mUid);
			
			/**
			 * Отправляем реквесты на сервер разделив их знаком $, чтобы можно было отправлять несколько запросов одним пакетом
			 */
			writeUTFBytes(JSON.stringify({method: method, timestamp: ts, params: params}) + "$");
			flush();
		}
		
		private function configureListeners() : void
		{
			addEventListener(Event.CLOSE, closeHandler);
			addEventListener(Event.CONNECT, connectHandler);
			addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
			addEventListener(ProgressEvent.SOCKET_DATA, socketDataHandler);
		}
		
		private function readResponse() : void
		{
			var str : String = readUTFBytes(bytesAvailable);
			var arr : Array /* of String */ = str.split("$");
			for each (str in arr)
			{
				if (str != "")
				{
					var xml : XML = new XML(str);
					if (xml.name() == "error")
					{
						CConsole.log(xml.toXMLString());
					}
					else
					{
						_mQueue[parseInt(xml.@timestamp)](xml);
						delete _mQueue[parseInt(xml.@timestamp)];
					}
					
				}
			}
		}
		
		private function closeHandler(event : Event) : void
		{
			trace("closeHandler: " + event);
		}
		
		private function connectHandler(event : Event) : void
		{
			trace("connectHandler: " + event);
		}
		
		private function ioErrorHandler(event : IOErrorEvent) : void
		{
			CConsole.log("Ошибка соединения с сервером");
		}
		
		private function securityErrorHandler(event : SecurityErrorEvent) : void
		{
			CConsole.log("Ошибка соединения с сервером");
		}
		
		private function socketDataHandler(event : ProgressEvent) : void
		{
			readResponse();
		}
	}
}
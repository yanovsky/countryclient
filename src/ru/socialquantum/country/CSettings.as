package ru.socialquantum.country
{
	import flash.errors.IllegalOperationError;
	
	/**
	 * Настройки игрового процесса
	 * @author Maxim I. Yanovsky
	 */
	public class CSettings
	{
		static private var _inited : Boolean = false;
		static private var _mAvailiblePlants : Vector.<String>;
		static private var _mPlantReadyState : int;
		static private var _mPlantDefaultState : int;
		
		public function CSettings()
		{
		
		}
		
		public static function init(data : XML) : void
		{
			if (_inited)
				throw new IllegalOperationError("Settings already inited");
			
			_inited = true;
			
			_mPlantReadyState = int(data.plant.ready_state);
			_mPlantDefaultState = int(data.plant.default_state);
			
			_mAvailiblePlants = new Vector.<String>();
			for each (var plantName : String in data.plant.availible..name)
			{
				_mAvailiblePlants.push(plantName);
			}
		
		}
		
		/**
		 * Список растений доступных к посадке
		 */
		public static function get availiblePlants() : Vector.<String>
		{
			return _mAvailiblePlants;
		}
		
		/**
		 * Цифра состояния созревшего растения
		 */
		static public function get plantReadyState() : int
		{
			return _mPlantReadyState;
		}
		
		/**
		 * Цифра состояния свежепосаженного растения
		 */
		static public function get plantDefaultState() : int
		{
			return _mPlantDefaultState;
		}
	}

}
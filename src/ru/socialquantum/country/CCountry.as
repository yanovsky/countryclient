package ru.socialquantum.country
{
	import flash.display.Sprite;
	import flash.events.Event;
	import ru.socialquantum.country.events.CUiEvent;
	import ru.socialquantum.country.models.CField;
	import ru.socialquantum.country.views.CFieldView;
	
	/**
	 * Основной класс модуля Country
	 * @author Maxim I. Yanovsky
	 */
	public class CCountry extends Sprite
	{
		private var _mSocket : CCustomSocket;
		private var _mUi : CUi;
		private var _mField : CField;
		private var _mView : CFieldView;
		private var _mLoginScreen : CLoginScreen;
		
		public function CCountry()
		{
			_mLoginScreen = new CLoginScreen();
			_mLoginScreen.addEventListener(CUiEvent.LOGIN_CLICK, onLogin);
			addChild(_mLoginScreen);
			addChild(CConsole.getInstance());
		}
		
		private function onLogin(e : CUiEvent) : void
		{
			_mLoginScreen.parent.removeChild(_mLoginScreen);
			_mSocket = new CCustomSocket(CLoginScreen(e.currentTarget).uid, 'localhost', 8080);
			_mSocket.send("fieldGet", onResponse);
			_mSocket.send("settingsGet", onSettingsGet);
		}
		
		private function onResponse(resp : XML) : void
		{
			_mField = new CField(resp);
			_mView = new CFieldView(_mField);
			addChild(_mView);
			addChild(CConsole.getInstance());
		}
		
		private function onSettingsGet(resp : XML) : void
		{
			CSettings.init(resp);
			_mUi = new CUi();
			addChild(_mUi);
			addChild(CConsole.getInstance());
			new CController(_mUi, _mField, _mView, _mSocket);
		}
	}

}
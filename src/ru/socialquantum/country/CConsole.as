package ru.socialquantum.country 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	/**
	 * Класс отображения информационной консоли в игре. Сначала скрыта, отображается только при первом сообщении в нее.
	 * Если игровой процесс идет без ошибок, отображаться не должна
	 * @author Maxim I. Yanovsky
	 */
	public class CConsole extends Sprite
	{
		private static var _mInstance : CConsole;
		private var _mTf : TextField = new TextField();
		
		public function CConsole()
		{
			_mTf.height = 100;
			_mTf.defaultTextFormat = new TextFormat("Courier", 10, 0xFFFFFF);
			_mTf.background = true;
			_mTf.backgroundColor = 0;
			_mTf.visible = false;
			_mTf.wordWrap = true;
			addChild(_mTf);
			
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		public static function getInstance() : CConsole
		{
			if (_mInstance == null)
			{
				_mInstance = new CConsole();
			}
			return _mInstance;
		}
		
		public static function log(msg : String) : void
		{
			getInstance().log(msg);
		}
		
		private function onAddedToStage(e : Event) : void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			_mTf.y = stage.stageHeight - _mTf.height;
			_mTf.width = stage.stageWidth;
		}
		
		private function log(msg : String) : void
		{
			_mTf.visible = true;
			_mTf.appendText(msg);
			_mTf.appendText("\n");
			_mTf.scrollV = _mTf.maxScrollV;
		}
	
	}

}
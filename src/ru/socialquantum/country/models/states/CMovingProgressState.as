package ru.socialquantum.country.models.states
{
	import ru.socialquantum.country.models.CPlant;
	import ru.socialquantum.country.models.states.base.CGameState;
	
	/**
	 * Состояние перемещения выбранного объекта. Клик по свободной клетке приведет к пересаживанию объекта в нее
	 * @author Maxim I. Yanovsky
	 */
	public class CMovingProgressState extends CGameState
	{
		private var _mPlant : CPlant
		
		public function CMovingProgressState(plant : CPlant)
		{
			super();
			_mPlant = plant;
		}
		
		public function get plant() : CPlant
		{
			return _mPlant;
		}
	
	}

}
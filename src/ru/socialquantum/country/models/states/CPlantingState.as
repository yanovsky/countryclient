package ru.socialquantum.country.models.states
{
	import ru.socialquantum.country.models.states.base.CGameState;
	
	/**
	 *  Состояние посадки растения. Любой клик по свободной клетке приведет к посадке.
	 * @author Maxim I. Yanovsky
	 */
	public class CPlantingState extends CGameState
	{
		private var _mPlantName : String;
		
		public function CPlantingState(plantName : String)
		{
			super();
			_mPlantName = plantName;
		}
		
		public function get plantName() : String
		{
			return _mPlantName;
		}
	
	}

}
package ru.socialquantum.country.models.states
{
	import ru.socialquantum.country.models.states.base.CGameState;
	
	/**
	 * Состояние выбора объекта для перемещения. Клик по занятой клетке приведет к изменению состояния на CMovingProgressState
	 * @author Maxim I. Yanovsky
	 */
	public class CMovingState extends CGameState
	{
		
		public function CMovingState()
		{
		
		}
	
	}

}
package ru.socialquantum.country.models.states
{
	import ru.socialquantum.country.models.states.base.CGameState;
	
	/**
	 * Состояние удаления (выкорчевывания). Любой клик по занятой клетке приведет к удалению растения.
	 * @author Maxim I. Yanovsky
	 */
	public class CRemovingState extends CGameState
	{
		
		public function CRemovingState()
		{
		
		}
	
	}

}
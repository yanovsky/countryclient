package ru.socialquantum.country.models.states
{
	import ru.socialquantum.country.models.states.base.CGameState;
	
	/**
	 * Обычное состояние (дефолтное). При нем можно только драгать карту.
	 * @author Maxim I. Yanovsky
	 */
	public class CNormalState extends CGameState
	{
		
		public function CNormalState()
		{
		
		}
	
	}

}
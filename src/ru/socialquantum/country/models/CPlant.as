package ru.socialquantum.country.models
{
	
	/**
	 * Модель растения
	 * @author Maxim I. Yanovsky
	 */
	public class CPlant
	{
		
		private var _mId : String;
		private var _mState : int;
		private var _mX : int;
		private var _mY : int;
		private var _mName : String;
		
		public function CPlant(data : XML = null)
		{
			if (data)
			{
				_mId = data.@id;
				_mState = data.@state;
				_mX = data.@x;
				_mY = data.@y;
				_mName = data.name();
			}
		
		}
		
		/**
		 * уникальный идентификатор растения
		 */
		public function get id() : String
		{
			return _mId;
		}
		
		/**
		 * состояние спелости растения
		 */
		public function get state() : int
		{
			return _mState;
		}
		
		/**
		 * координата x в системе координат поля
		 */
		public function get x() : int
		{
			return _mX;
		}
		
		/**
		 * координата y в системе координат поля
		 */
		public function get y() : int
		{
			return _mY;
		}
		
		/**
		 * имя класса растения (картофель, морковь)
		 */
		public function get name() : String
		{
			return _mName;
		}
	
	}

}
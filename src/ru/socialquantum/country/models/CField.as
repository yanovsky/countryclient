package ru.socialquantum.country.models
{
	import flash.geom.Rectangle;
	
	/**
	 * Модель поля вместе со всеми растениями внутри
	 * @author Maxim I. Yanovsky
	 */
	public class CField
	{
		
		private var _mX : int;
		private var _mY : int;
		private var _mWidth : int;
		private var _mHeight : int;
		private var _mPlants : Vector.<CPlant>;
		private var _mId : String;
		private var _mRect : Rectangle;
		
		public function CField(data : XML)
		{
			_mX = data.@x;
			_mY = data.@y;
			_mWidth = data.@width;
			_mHeight = data.@height;
			_mId = data.@id;
			_mPlants = new Vector.<CPlant>;
			for each (var node : XML in data.children())
			{
				_mPlants.push(new CPlant(node));
			}
			_mRect = new Rectangle(x, y, width, height);
		}
		
		/**
		 * Получение растения по его id
		 * @param	id
		 * @return
		 */
		public function getPlantById(id : String) : CPlant
		{
			for each (var plant : CPlant in plants)
			{
				if (plant.id == id)
					return plant;
			}
			return null;
		}
		
		/**
		 * x левого-верхнего угла поля в системе координат ячеек
		 */
		public function get x() : int
		{
			return _mX;
		}
		
		/**
		 * y левого-верхнего угла поля в системе координат ячеек
		 */
		public function get y() : int
		{
			return _mY;
		}
		
		/**
		 * ширина поля в ячейках
		 */
		public function get width() : int
		{
			return _mWidth;
		}
		
		/**
		 * высота поля в ячейках
		 */
		public function get height() : int
		{
			return _mHeight;
		}
		
		/**
		 * растения на поле
		 */
		public function get plants() : Vector.<CPlant>
		{
			return _mPlants;
		}
		
		/**
		 * уникальный идентификатор поля
		 */
		public function get id() : String
		{
			return _mId;
		}
		
		/**
		 * прямоугольник поля
		 */
		public function get rect() : Rectangle
		{
			return _mRect;
		}
	}

}
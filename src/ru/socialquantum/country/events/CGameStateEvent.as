package ru.socialquantum.country.events
{
	import flash.events.Event;
	import ru.socialquantum.country.models.states.base.CGameState;
	
	/**
	 * Класс событий связанных с изменением состояния игрового контроллера
	 * @author Maxim I. Yanovsky
	 */
	public class CGameStateEvent extends Event
	{
		/**
		 * Игрок выбрал какое-то действие в списке действий
		 */
		public static const STATE_CHANGE : String = "CGameStateEvent.STATE_CHANGE";
		
		private var _mState : CGameState;
		
		public function CGameStateEvent(type : String, state : CGameState, bubbles : Boolean = false, cancelable : Boolean = false)
		{
			super(type, bubbles, cancelable);
			_mState = state;
		}
		
		
		public override function clone() : Event
		{
			return new CGameStateEvent(type, state, bubbles, cancelable);
		}
		
		public override function toString() : String
		{
			return formatToString("CGameStateEvent", "type", "mState", "bubbles", "cancelable", "eventPhase");
		}
		
		public function get state():CGameState 
		{
			return _mState;
		}
	
	}

}
package ru.socialquantum.country.events
{
	import flash.events.Event;
	
	/**
	 * События взаимодействия игрока с UI
	 * @author Maxim I. Yanovsky
	 */
	public class CUiEvent extends Event
	{
		/**
		 * Игрок нажал кнопку "Сделать ход"
		 */
		static public const TURN_BUTTON_CLICK : String = "CUiEvent.TURN_BUTTON_CLICK";
		/**
		 * Игрок ввел имся и нажал кнопку Login (на первом экране)
		 */
		static public const LOGIN_CLICK : String = "CUiEvent.LOGIN_CLICK";
		
		public function CUiEvent(type : String, bubbles : Boolean = false, cancelable : Boolean = false)
		{
			super(type, bubbles, cancelable);
		}
		
		public override function clone() : Event
		{
			return new CUiEvent(type, bubbles, cancelable);
		}
		
		public override function toString() : String
		{
			return formatToString("CUiEvent", "type", "bubbles", "cancelable", "eventPhase");
		}
	
	}

}
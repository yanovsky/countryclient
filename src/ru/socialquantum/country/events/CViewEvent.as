package ru.socialquantum.country.events
{
	import flash.events.Event;
	
	/**
	 * События взаимодействия игрока с игровым миром
	 * @author Maxim I. Yanovsky
	 */
	public class CViewEvent extends Event
	{
		/**
		 * Игрок кликнул по свободной клетке на поле
		 */
		public static const CLICK_ON_CELL : String = "CViewEvent.CLICK_ON_CELL";
		
		/**
		 * Игрок кликнул по растению на поле
		 */
		public static const CLICK_ON_PLANT : String = "CViewEvent.CLICK_ON_PLANT";
		
		public function CViewEvent(type : String, bubbles : Boolean = false, cancelable : Boolean = false)
		{
			super(type, bubbles, cancelable);
		}
		
		public override function clone() : Event
		{
			return new CViewEvent(type, bubbles, cancelable);
		}
		
		public override function toString() : String
		{
			return formatToString("CViewEvent", "type", "bubbles", "cancelable", "eventPhase");
		}
	
	}

}
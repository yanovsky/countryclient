package ru.socialquantum.country.views
{
	import caurina.transitions.Tweener;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.Dictionary;
	import ru.socialquantum.country.events.CViewEvent;
	import ru.socialquantum.country.models.CField;
	import ru.socialquantum.country.models.CPlant;
	import ru.socialquantum.country.utils.CBitmapStorage;
	import ru.socialquantum.country.utils.CIsometric;
	
	/**
	 * Класс отображения игрового поля.
	 * Содержит логику:
	 * Наведения мыши на ячейки
	 * Перемещения растений
	 * @author Maxim I. Yanovsky
	 */
	public class CFieldView extends Sprite
	{
		private var _mOveredCell : Point; //клетка на которую наведена мышь
		private var _mOveredPlant : CPlantView; //растение на которое наведена мышь
		
		protected var _mData : CField;
		protected var _mMainCont : Sprite = new Sprite(); //основной контейнер
		protected var _mBackground : Sprite = new Sprite(); //контейнер фона
		protected var _mPlantCont : Sprite = new Sprite(); //контейнер ячеек
		protected var _mOveredCellCont : Sprite = new Sprite(); //контейнер для подстветки ячеек
		protected var _mMovingCont : Sprite = new Sprite(); //контейнер для перемещаемого предмета
		protected var _mDragRect : Rectangle;
		protected var _mPlants : Vector.<Vector.<CPlantView>> = new Vector.<Vector.<CPlantView>>(); //двухмерный массив растений от координат
		protected var _mPlantsHeap : Vector.<CPlantView> = new Vector.<CPlantView>; //все растения в одной куче
		
		private var _mMovingPlant : CPlant;
		private var _mMovingPlantView : CPlantView;
		private var _mMovingItem : Sprite = new Sprite();
		
		public function CFieldView(data : CField)
		{
			this._mData = data;
			
			addChild(_mMainCont);
			_mMainCont.addChild(_mBackground);
			_mMainCont.addChild(_mPlantCont);
			_mMainCont.addChild(_mMovingCont);
			_mMainCont.addChild(_mOveredCellCont);
			
			_mMovingCont.mouseChildren = false
			_mMovingCont.mouseEnabled = false;
			_mOveredCellCont.mouseChildren = false;
			_mOveredCellCont.mouseEnabled = false;
			
			_mPlantCont.y += (_mData.width + _mData.x) * CIsometric.CELL_HEIGHT / 2;
			_mOveredCellCont.y = _mPlantCont.y;
			_mOveredCellCont.x = _mPlantCont.x;
			
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			addEventListener(MouseEvent.CLICK, onClick);
		}
		
		/**
		 * Полное обновление отображения
		 * @param	data
		 */
		public function update(data : CField) : void
		{
			var plant : CPlant;
			var plantView : CPlantView;
			var needRender : Boolean = false;
			
			//проверим, нет ли в текущем поле лишних объектов
			for each (plant in _mData.plants)
			{
				if (data.getPlantById(plant.id) == null)
					removePlantView(getPlantViewByPlant(plant));
			}
			
			//обновим все объекты
			for each (plant in data.plants)
			{
				plantView = getPlantViewByPlant(plant);
				if (plantView == null)
				{
					addPlantView(plant);
					needRender = true;
				}
				else
					plantView.update(plant);
			}
			
			if (needRender)
			{
				render();
			}
			
			this._mData = data;
		}
		
		/**
		 * Функция которая включает состояние перетаскивания посадки
		 * @param	plant
		 */
		public function moveCPlant(plant : CPlant) : void
		{
			/**
			 * Перемещение объектов реализовано следующим образом:
			 * Запоминаем и удаляем с карты перемещаемый объект
			 * Создаем его образ (битмапу) на слое _mMovingCont и начинаем водить за мышью
			 * Если игрок отменяет действие - восстанавливаем объект
			 */
			_mMovingPlant = plant;
			_mMovingPlantView = getPlantViewByPlant(plant);
			
			while (_mMovingItem.numChildren)
				_mMovingItem.removeChildAt(0);
			
			var bitmap : Bitmap = new Bitmap(_mMovingPlantView.bitmap.bitmapData);
			_mMovingItem.addChild(bitmap);
			_mMovingCont.addChild(_mMovingItem);
			
			var pnt : Point = _mMovingCont.globalToLocal(new Point(stage.mouseX, stage.mouseY));
			_mMovingItem.x = pnt.x;
			_mMovingItem.y = pnt.y;
			
			pnt = _mMovingItem.globalToLocal(_mMovingPlantView.localToGlobal(new Point(_mMovingPlantView.bitmap.x, _mMovingPlantView.bitmap.y)));
			removePlantView(_mMovingPlantView);
			bitmap.x = pnt.x;
			bitmap.y = pnt.y;
			Tweener.addTween(bitmap, {y: bitmap.y - 50, time: 1});
		}
		
		/**
		 * Функция которая завершает состояние перетаскивания посадки
		 */
		public function compeleteMoving() : void
		{
			while (_mMovingItem.numChildren)
				_mMovingItem.removeChildAt(0);
			_mMovingPlantView.onMouseOut();
			_mMovingPlant = null;
		}
		
		/**
		 * Функция которая отменяет состояние перетаскивания посадки
		 */
		public function cancelMoving() : void
		{
			while (_mMovingItem.numChildren)
				_mMovingItem.removeChildAt(0);
			addPlantView(_mMovingPlant);
			_mMovingPlant = null;
			render();
		
		}
		
		private function onClick(e : MouseEvent) : void
		{
			if (overedPlant)
			{
				overedPlant.onClick();
			}
			else if (overedCell)
			{
				dispatchEvent(new CViewEvent(CViewEvent.CLICK_ON_CELL, true));
				unHinghLightCells();
			}
		}
		
		private function onMouseMove(e : MouseEvent) : void
		{
			/**
			 * Нахождение ячейки/растения под курсором мыши.
			 * Нужен, чтобы обойти проблему перекрытия объектов прозрачностями Bitmap-ы
			 *
			 * Сначала находим претендентов на которых может быть наведена мышь, потом перебором определяем на какой именно.
			 *
			 * Тут же вызываем необходимые mouseOver и mouseOut
			 */
			
			checkMovingItem(e);
			
			var pnt : Point = _mPlantCont.globalToLocal(new Point(e.stageX, e.stageY));
			pnt = CIsometric.isometricToNormal(pnt);
			pnt.x = Math.floor(pnt.x);
			pnt.y = Math.floor(pnt.y);
			
			var plantView : CPlantView = getPlantByXY(pnt.x, pnt.y);
			var pretendents : Vector.<CPlantView> = new Vector.<CPlantView>();
			pretendents = pretendents.concat(getPretendents(pnt.x, pnt.y));
			pretendents = pretendents.concat(getPretendents(pnt.x, pnt.y - 1));
			pretendents = pretendents.concat(getPretendents(pnt.x + 1, pnt.y));
			var overed : Vector.<CPlantView> = new Vector.<CPlantView>;
			
			for each (plantView in pretendents)
			{
				if (plantView.checkMouseOver(e))
				{
					overed.push(plantView);
				}
			}
			if (overed.length)
			{
				unHinghLightCells();
				_mOveredCell = null;
				
				overed.sort(_mPlantsSortFunction);
				plantView = overed[overed.length - 1];
				if (plantView != overedPlant)
				{
					if (overedPlant)
					{
						overedPlant.onMouseOut();
					}
					_mOveredPlant = plantView;
					overedPlant.onMouseOver();
				}
			}
			else
			{
				if (overedPlant)
				{
					overedPlant.onMouseOut();
				}
				_mOveredPlant = null;
				
				if (_mData.rect.containsPoint(pnt))
				{
					if (overedCell == null || !overedCell.equals(pnt))
					{
						unHinghLightCells();
						highLightCell(pnt);
					}
				}
				else
				{
					unHinghLightCells();
					_mOveredCell = null;
				}
				
			}
		}
		
		private function getPretendents(x : Number, y : Number) : Vector.<CPlantView>
		{
			/**
			 * Идем вниз по столбику из клеток и смотрим, какие растения могут своей высотой перекрыть нашу клетку
			 */
			
			var retval : Vector.<CPlantView> = new Vector.<CPlantView>();
			
			var x_ : int = x;
			var y_ : int = y;
			
			for (x_, y_; x_ >= _mData.x && (y_ - y) * CIsometric.CELL_HEIGHT < CPlantView.MAX_HEIGHT; x_--, y_++)
			{
				var _mPlantsView : CPlantView = getPlantByXY(x_, y_);
				if (_mPlantsView && _mPlantsView.bitmap.height > (y_ - y) * CIsometric.CELL_HEIGHT)
				{
					retval.push(_mPlantsView);
				}
			}
			return retval;
		}
		
		private function getPlantByXY(x : Number, y : Number) : CPlantView
		{
			if (!_mData.rect.contains(x, y))
				return null;
			return _mPlants[x][y];
		}
		
		private function unHinghLightCells() : void
		{
			_mOveredCellCont.graphics.clear();
		}
		
		private function highLightCell(pnt : Point) : void
		{
			_mOveredCell = pnt;
			_mOveredCellCont.graphics.beginFill(0x00FF00, 0.2);
			CIsometric.drawIsometricRect(_mOveredCellCont.graphics, new Rectangle(pnt.x, pnt.y, 1, 1));
		}
		
		private function checkMovingItem(e : MouseEvent) : void
		{
			if (_mMovingItem.numChildren)
			{
				var p : Point = globalToLocal(new Point(e.stageX, e.stageY));
				_mMovingItem.x = p.x;
				_mMovingItem.y = p.y;
			}
		}
		
		private function onMouseDown(e : MouseEvent) : void
		{
			_mMainCont.startDrag(false, _mDragRect);
		}
		
		private function onMouseUp(e : Event) : void
		{
			_mMainCont.stopDrag();
		}
		
		private function onAddedToStage(e : Event) : void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			CBitmapStorage.getBitmapData("BG.jpg", onBitmapLoaded);
		}
		
		private function onBitmapLoaded(bdata : BitmapData) : void
		{
			initBackground(bdata);
			
			drawCells();
			
			initPlants();
			
			render();
		}
		
		private function initBackground(bdata : BitmapData) : void
		{
			var rect : Rectangle = new Rectangle(0, 0, _mData.width + _mData.x, _mData.height + _mData.y);
			var width : Number = Math.max(stage.stageWidth, CIsometric.normalToIsometric(rect.bottomRight).x);
			var leftBottom:Point = CIsometric.normalToIsometric(new Point(rect.left, rect.bottom));
			var rightTop:Point = CIsometric.normalToIsometric(new Point(rect.right, rect.top))
			var height : Number = Math.max(stage.stageHeight, leftBottom.y - rightTop.y);
			
			_mBackground.graphics.beginBitmapFill(bdata);
			_mBackground.graphics.drawRect(0, 0, width, height);
			_mBackground.graphics.endFill();
			
			_mDragRect = new Rectangle(-(width - stage.stageWidth), -(height - stage.stageHeight), width - stage.stageWidth, height - stage.stageHeight);
		}
		
		private function render() : void
		{
			_mPlantsHeap.sort(_mPlantsSortFunction);
			for each (var _mPlantsView : CPlantView in _mPlantsHeap)
			{
				_mPlantCont.addChild(_mPlantsView);
			}
		}
		
		private function _mPlantsSortFunction(plant1 : CPlantView, plant2 : CPlantView) : int
		{
			if (plant1.y > plant2.y)
				return 1;
			if (plant1.y == plant2.y)
				return 0;
			return -1;
		}
		
		private function initPlants() : void
		{
			for (var x : int = 0; x < _mData.x + _mData.width; x++)
			{
				_mPlants[x] = new Vector.<CPlantView>();
				for (var y : int = 0; y < _mData.y + _mData.height; y++)
				{
					_mPlants[x][y] = null;
				}
			}
			for each (var plant : CPlant in _mData.plants)
			{
				addPlantView(plant);
			}
		}
		
		private function drawCells() : void
		{
			_mPlantCont.graphics.lineStyle(1, 0x006600, 0.2);
			for (var x : int = _mData.x; x < _mData.x + _mData.width; x++)
			{
				for (var y : int = _mData.y; y < _mData.y + _mData.height; y++)
				{
					CIsometric.drawIsometricRect(_mPlantCont.graphics, new Rectangle(x, y, 1, 1));
				}
			}
			_mPlantCont.graphics.lineStyle(3, 0x00AA00);
			CIsometric.drawIsometricRect(_mPlantCont.graphics, new Rectangle(_mData.x, _mData.y, _mData.width, _mData.height));
		}
		
		private function addPlantView(plant : CPlant) : void
		{
			var plantView : CPlantView = new CPlantView(plant);
			var pnt : Point = CIsometric.normalToIsometric(new Point(plant.x, plant.y));
			plantView.x = pnt.x;
			plantView.y = pnt.y;
			
			_mPlants[plant.x][plant.y] = plantView;
			_mPlantsHeap.push(plantView);
		}
		
		private function removePlantView(plantView : CPlantView) : void
		{
			plantView.parent.removeChild(plantView);
			var plant : CPlant = plantView.plant;
			_mPlants[plant.x][plant.y] = null;
			_mPlantsHeap.splice(_mPlantsHeap.indexOf(plantView), 1);
		}
		
		private function getPlantViewByPlant(plant : CPlant) : CPlantView
		{
			if (plant == null)
				return null;
			if (_mPlants[plant.x] && _mPlants[plant.x][plant.y])
				return _mPlants[plant.x][plant.y];
			return null;
		}
		
		public function get overedCell() : Point
		{
			return _mOveredCell;
		}
		
		public function get overedPlant() : CPlantView
		{
			return _mOveredPlant;
		}
	}

}
package ru.socialquantum.country.views
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.GlowFilter;
	import flash.geom.Point;
	import ru.socialquantum.country.events.CViewEvent;
	import ru.socialquantum.country.models.CPlant;
	import ru.socialquantum.country.utils.CBitmapStorage;
	import ru.socialquantum.country.utils.CIsometric;
	/**
	 * Класс отображения растения на поле
	 * @author Maxim I. Yanovsky
	 */
	[Event(name="CViewEvent.CLICK_ON_PLANT",type="ru.socialquantum.country.events.CViewEvent")]
	
	public class CPlantView extends Sprite
	{
		/**
		 * Максимальная высота растения в пикселях, обновляется при инициализации/апдейте графики
		 */
		public static var MAX_HEIGHT : Number = 0;
		private static const OVERED_FILTER : GlowFilter = new GlowFilter(0xFF9900, 1, 10, 10, 6);
		
		private var _mPlant : CPlant;
		private var _mBitmap : Bitmap = new Bitmap();
		
		public function CPlantView(plant : CPlant)
		{
			addChild(_mBitmap);
			update(plant);
		}
		
		/**
		 * Поверка, наведен ли курсор мыши на эту ячейку
		 * @param	e событие пришедшее какому-либо слушателю событий мыши
		 * @return
		 */
		public function checkMouseOver(e : MouseEvent) : Boolean
		{
			if (_mBitmap.bitmapData)
			{
				var pnt : Point = localToGlobal(new Point(_mBitmap.x, _mBitmap.y));
				return _mBitmap.bitmapData.hitTest(pnt, 0, new Point(e.stageX, e.stageY));
			}
			else
			{
				return false;
			}
		
		}
		
		public function onMouseOut(e : Event = null) : void
		{
			_mBitmap.filters = null;
		}
		
		public function onMouseOver(e : Event = null) : void
		{
			_mBitmap.filters = [OVERED_FILTER];
		}
		
		/**
		 * Функция обновления отображения ячейки
		 * @param	new_plant - растение которое должно отобразится
		 */
		public function update(new_plant : CPlant) : void
		{
			if (plant == null || plant.state != new_plant.state)
			{
				CBitmapStorage.getBitmapData("plants/" + new_plant.name + "/" + new_plant.state + ".PNG", setBitmap);
			}
			_mPlant = new_plant;
		}
		
		public function onClick(e : Event = null) : void
		{
			dispatchEvent(new CViewEvent(CViewEvent.CLICK_ON_PLANT, true));
		}
		
		public function get plant() : CPlant
		{
			return _mPlant;
		}
		
		public function get bitmap() : Bitmap
		{
			return _mBitmap;
		}
		
		private function setBitmap(bdata : BitmapData) : void
		{
			_mBitmap.bitmapData = bdata;
			_mBitmap.y = CIsometric.CELL_HEIGHT / 2 - bdata.height;
			
			if (bdata.height > MAX_HEIGHT)
			{
				MAX_HEIGHT = bdata.height;
			}
		}
	}
}
package ru.socialquantum.country
{
	import flash.events.Event;
	import flash.geom.Point;
	import ru.socialquantum.country.events.CGameStateEvent;
	import ru.socialquantum.country.events.CUiEvent;
	import ru.socialquantum.country.events.CViewEvent;
	import ru.socialquantum.country.models.CField;
	import ru.socialquantum.country.models.CPlant;
	import ru.socialquantum.country.models.states.base.CGameState;
	import ru.socialquantum.country.models.states.CHarvestingState;
	import ru.socialquantum.country.models.states.CMovingProgressState;
	import ru.socialquantum.country.models.states.CMovingState;
	import ru.socialquantum.country.models.states.CNormalState;
	import ru.socialquantum.country.models.states.CPlantingState;
	import ru.socialquantum.country.models.states.CRemovingState;
	import ru.socialquantum.country.views.CFieldView;
	import ru.socialquantum.country.views.CPlantView;
	
	/**
	 * Контроллер игры. Именно здесь принимаются решения об отправке тех или иных сообщений на сервер.
	 * Принцип действия:
	 * Получили евент из UI --->>> Изменили state и/или отправили запрос на сервер
	 * Получили евент из View --->>> В зависимости от state приняли решение и отправили запрос на сервер
	 * Получили ответ от сервера --->>> Обновляем _mView
	 * @author Maxim I. Yanovsky
	 */
	
	public class CController
	{
		private var _mUi : CUi;
		private var _mField : CField;
		private var _mView : CFieldView;
		private var _mState : CGameState = new CNormalState();
		private var _mSocket : CCustomSocket;
		
		public function CController(ui : CUi, field : CField, view : CFieldView, socket : CCustomSocket)
		{
			this._mUi = ui;
			this._mField = field;
			this._mView = view;
			this._mSocket = socket;
			
			_mUi.addEventListener(CGameStateEvent.STATE_CHANGE, onStateChange);
			_mUi.addEventListener(CUiEvent.TURN_BUTTON_CLICK, onTurnBtnClick);
			
			_mView.addEventListener(CViewEvent.CLICK_ON_CELL, onCellClick);
			_mView.addEventListener(CViewEvent.CLICK_ON_PLANT, onPlantClick);
		}
		
		private function onTurnBtnClick(e : Event) : void
		{
			_mSocket.send("fieldGrowth", onFieldChanged);
		}
		
		private function onPlantClick(e : CViewEvent) : void
		{
			var plant : CPlant = (e.target as CPlantView).plant;
			var pnt : Point;
			
			if (_mState is CRemovingState)
			{
				_mSocket.send("plantRemove", onFieldChanged, plant.id);
			}
			else if (_mState is CHarvestingState)
			{
				if (plant.state != CSettings.plantReadyState)
				{
					trace("еще не созрело");
					return;
				}
				_mSocket.send("plantRemove", onFieldChanged, plant.id);
			}
			else if (state is CMovingState)
			{
				_mState = new CMovingProgressState(plant);
				_mView.moveCPlant(plant);
			}
		
		}
		
		private function onCellClick(e : CViewEvent) : void
		{
			var pnt : Point = _mView.overedCell;
			if (_mState is CPlantingState)
			{
				_mSocket.send("plantAdd", onFieldChanged, (state as CPlantingState).plantName, pnt.x, pnt.y);
			}
			else if (_mState is CMovingProgressState)
			{
				var plant : CPlant = (_mState as CMovingProgressState).plant;
				if (plant.x != pnt.x || plant.y != pnt.y)
				{
					_mView.compeleteMoving();
					_mSocket.send("plantMove", onFieldChanged, plant.id, pnt.x, pnt.y);
				}
				else
				{
					_mView.cancelMoving();
				}
				_mState = new CMovingState();
			}
		}
		
		/**
		 * Вот тут и происходит загрузка новых данных с сервера во _mView
		 * @param	resp ответ от сервера
		 */
		private function onFieldChanged(resp : XML) : void
		{
			var newField : CField = new CField(resp);
			_mView.update(newField);
		}
		
		private function onStateChange(e : CGameStateEvent) : void
		{
			applyState(e.state);
		}
		
		private function get state() : CGameState
		{
			return _mState;
		}
		
		/**
		 * Сеттер изменения игрового состояния
		 */
		private function applyState(value : CGameState) : void
		{
			if (value is CNormalState && _mState is CMovingProgressState)
			{
				//если нажали кнопку "отменить" когда уже был выбран предмет для перемещения
				_mView.cancelMoving();
			}
			_mState = value;
		}
	
	}

}
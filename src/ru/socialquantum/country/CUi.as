package ru.socialquantum.country 
{
	import fl.controls.Button;
	import fl.controls.List;
	import flash.display.Sprite;
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import ru.socialquantum.country.CSettings;
	import ru.socialquantum.country.events.CGameStateEvent;
	import ru.socialquantum.country.events.CUiEvent;
	import ru.socialquantum.country.models.states.*;

	
	/**
	 * Контейнер для элементов пользовательского интерфейса в игре, таких как список возможных действий и кнопок
	 * @author Maxim I. Yanovsky
	 */
	
	[Event(name="CUiEvent.TURN_BUTTON_CLICK", type="ru.socialquantum.country.events.CUiEvent")]
	[Event(name="CGameStateEvent.STATE_CHANGE", type="ru.socialquantum.country.events.CGameStateEvent")]
	public class CUi extends Sprite
	{
		
		private var _mList : List;
		private var _mCancelBtn : Button;
		
		public function CUi()
		{
			_mList = new List();
			for each (var plantName : String in CSettings.availiblePlants)
			{
				_mList.addItem({label: "Садить " + plantName, state: new CPlantingState(plantName)});
			}
			_mList.addItem({label: "Удалять", state: new CRemovingState()});
			_mList.addItem({label: "Собирать", state: new CHarvestingState()});
			_mList.addItem({label: "Двигать", state: new CMovingState()});
			_mList.height = 120;
			_mList.addEventListener(Event.CHANGE, onActionSelect);
			addChild(_mList);
			
			var turnBtn : Button = new Button();
			turnBtn.label = "Сделать ход";
			turnBtn.x = 100;
			turnBtn.addEventListener(MouseEvent.CLICK, onTurnClick);
			addChild(turnBtn);
			
			_mCancelBtn = new Button();
			_mCancelBtn.label = "Отмена";
			_mCancelBtn.y = _mList.height;
			_mCancelBtn.addEventListener(MouseEvent.CLICK, onCancelClick);
			_mCancelBtn.visible = false;
			addChild(_mCancelBtn);
		}
		
		private function onCancelClick(e : Event) : void
		{
			_mCancelBtn.visible = false;
			_mList.clearSelection();
			dispatchEvent(new CGameStateEvent(CGameStateEvent.STATE_CHANGE, new CNormalState()));
		}
		
		private function onTurnClick(e : Event) : void
		{
			dispatchEvent(new CUiEvent(CUiEvent.TURN_BUTTON_CLICK));
		}
		
		private function onActionSelect(e : Event) : void
		{
			dispatchEvent(new CGameStateEvent(CGameStateEvent.STATE_CHANGE, _mList.selectedItem.state));
			_mCancelBtn.visible = true;
		}
	
	}

}
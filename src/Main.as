package 
{
	import flash.display.Sprite;
	import ru.socialquantum.country.CCountry;
	/**
	 * Точка входа в приложение
	 * @author Maxim I. Yanovsky
	 */
	public class Main extends Sprite 
	{
		
		
		public function Main():void 
		{
			addChild(new CCountry());
		}
		
		
	}
	
}